net use \\{{ HOSTNAME }}\C$ {{ PASSWORD }} /user:{{ USERNAME }}
XCOPY zabbix_agents_3.0.17.win\* \\{{ HOSTNAME }}\C$\MOSS\zabbix\ /K /E /H /Y
XCOPY run\{{ HOSTNAME }}_zabbix_agentd.win.conf \\{{ HOSTNAME }}\C$\MOSS\zabbix\conf\zabbix_agentd.win.conf*  /Y
net use \\{{ HOSTNAME }}\C$ /DELETE /Y
