'''
Created on May 29, 2018

@author: ekhavee
'''
import logging
from peewee import SqliteDatabase, Model, CharField, BooleanField, TextField,\
  IntegerField
from xlrd import open_workbook
from excelWriter import ExcelWriter
from ecutil import cph
import getpass
import jinja2
from os.path import expanduser
import os
import json

home = expanduser("~")
pwconf = os.path.join(home, ".pyauto")
USER  = "USERNAME"
PASS  = "PASSWORD"

class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())


sqliteDB = SqliteDatabase('cache.db', pragmas=[
    ['journal_mode', 'wal'],  # WAL-mode.
    ['cache_size', -64 * 1000],  # 64MB cache.
    ['synchronous', 0]
  ])

class InstallStatus(Model):
  HOSTNAME              = CharField(primary_key=True)
  NAME                  = CharField(null=True, index=True)
  ACCESSVIA             = CharField(null=True, index=True)
  IP                    = CharField(null=True, index=True)
  TOOLS                 = CharField(null=True, index=True)
  OS                    = CharField(null=True, index=True)
  DESC                  = CharField(null=True, index=True)
  OSVERSION             = CharField(null=True, index=True)
  AGENTEXIST            = CharField(null=True, index=True)
  DISKSIZE              = IntegerField(null=True, index=True)
  CREATEFLDRRC          = IntegerField(null=True, index=True)
  COPYFILERC            = IntegerField(null=True, index=True)
  SETUPRC               = IntegerField(null=True, index=True)
  SUDOFAILED            = BooleanField(null=True, index=True)
  OUTPUTLOG             = TextField(null=True, index=True)
  AUTHFAILED            = BooleanField(null=True, index=True)
  CONNECTFAILED         = BooleanField(null=True, index=True)
  ZBXOS                 = CharField(null=True, index=True)
  ARCH                  = CharField(null=True, index=True)
  class Meta:
    database = sqliteDB

  
def renderTemplate(templateFile, hst, auth):
  return jinja2.Environment(loader=jinja2.FileSystemLoader('./run/')).get_template(templateFile).render({
          'HOSTNAME': hst.HOSTNAME,
          'USERNAME': auth[USER],
          'PASSWORD': auth[PASS]
        }
      )


def readPWConf():
  data = {}
  if os.path.isfile(pwconf):
    data = json.load(open(pwconf))
  for a in data:
    data[a] = cph.dcpt(data[a])
  return data

def writePWConf(data):
  op = open(pwconf, "w")
  json.dump(data, op)
  op.close()
  
def addAuth(username):
  data = readPWConf()
  data[username] = cph.ecpt(getpass.getpass(prompt='Password: ', stream=None))
  writePWConf(data)
  
def clearAuth(username = None):
  data = readPWConf()
  if username in data:
    data.pop(username)
  if username == "ALL":
    data = {}
  writePWConf(data)
  
def readInputExcel(inputExcel, sheetName=None):
  book = open_workbook(inputExcel)
  sheet = None
  if sheetName == None:
    sheet = book.sheet_by_index(0)
  else:
    sheet = book.sheet_by_name(sheetName)
  keys = [sheet.cell(0, col_index).value for col_index in xrange(sheet.ncols)]
  dict_list = []
  for row_index in xrange(1, sheet.nrows):
    d = {keys[col_index]:sheet.cell(row_index, col_index).value for col_index in xrange(sheet.ncols)}
    dict_list.append(d)
    
  d = []
  for a in dict_list: 
    h,s = InstallStatus.get_or_create(HOSTNAME=a['HOSTNAME'])
    h.OS    = a['OS']
    h.IP    = a['IP']
    h.TOOLS = a['TOOLS']
    h.DESC  = a['DESC']
    h.save()
    d.append(h.HOSTNAME)
  return d

def writeExcelFromStatus(excelName):
  wb = ExcelWriter(excelName)

  cols = InstallStatus._meta.sorted_field_names
  worksheet = wb.getWorkSheetWCols("Details", cols)
  row = 2
  for a in InstallStatus.select().order_by(InstallStatus.HOSTNAME):
    col = 1
    for b in cols:
      wrap=False
      if b in ['OSVERSION', 'OUTPUTLOG']:
        wrap=True
      wb.writeCell(worksheet, row, col, getattr(a, b), wrap=wrap)
      col = col + 1
    row = row + 1
  wb.Close()