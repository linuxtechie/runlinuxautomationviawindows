#!/usr/local/bin/python2.7
# encoding: utf-8
from commonOs import InstallStatus, sqliteDB, readInputExcel, writeExcelFromStatus,\
  addAuth, clearAuth
from multiprocessing.dummy import Pool as ThreadPool 
import sys, os, logging, logging.config, multiprocessing, json
from argparse import ArgumentParser
from pyLinAuto import SSHExecutor
from pyWinAuto import WinExecutor

logging.config.dictConfig(json.load(open(os.path.join(os.path.dirname(__file__), "logging.config"))))

class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())



def OSExecutor(data):
  task = None
  if InstallStatus.get(HOSTNAME=data[0]).OS == "LINUX":
    task = SSHExecutor(data)
  elif InstallStatus.get(HOSTNAME=data[0]).OS == "WIN":
    task = WinExecutor(data)
  return task 

def GetData(data):
  task = OSExecutor(data)
  task.getData()
  task.close()
  
def InstallAgent(data):
  task = OSExecutor(data)
  task.getData()
  task.installAgent()
  task.close()

def InstallAgentForcefully(data):
  task = OSExecutor(data)
  task.getData()
  task.forcefully()
  task.installAgent()


def main(argv=None): # IGNORE:C0111
  with open(".pyLinAuto", "ab+") as f:
    f.write(' '.join(sys.argv) + '\r\n')

  if argv is None:
      argv = sys.argv
  else:
      sys.argv.extend(argv)

  parser = ArgumentParser()
  parser.add_argument("command")
  parser.add_argument("input")
  
  args = parser.parse_args()

  sqliteDB.connect()
  sqliteDB.create_tables([InstallStatus])

  InstallStatus.delete().execute()
  
  if args.command == "add-auth":
    addAuth(args.input)
    return
  elif args.command == "clear-auth":
    if args.input.upper() == "ALL":
      clearAuth()
    else:
      clearAuth(args.input)
    return
    

  method2Call = globals()[args.command]
  lock = multiprocessing.Lock()  

  if 1 > 2:
    H = "pose01mw0125v.prod.sdt.ericsson.se"
    I = "172.29.14.8"
    O = "WIN"
    T = "TEST"
    h,s = InstallStatus.get_or_create(HOSTNAME=H)
    h.IP = I
    h.TOOLS = T
    h.OS = O
    h.save()
    GetData([H, lock])
  else:
    d = []
    for hst in readInputExcel(args.input):
      d.append((hst, lock))
    pool = ThreadPool(50)
    pool.map(method2Call, d)
    pool.close()
    pool.join()
  writeExcelFromStatus("InstallStatus.xlsx")
  sqliteDB.close()
if __name__ == "__main__":
  sys.exit(main())