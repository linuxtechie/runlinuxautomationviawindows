'''
Created on Sep 4, 2018

@author: ekhavee
'''

import base64

from Crypto import Random
from Crypto.Cipher import AES

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[0:-ord(s[-1])]


class AESCipher:
  def __init__( self, key ):
    self.key = key

  def ecpt( self, raw ):
    raw = pad(raw)
    iv = Random.new().read( AES.block_size )
    cph = AES.new( self.key, AES.MODE_CBC, iv )
    return base64.b64encode( iv + cph.encrypt( raw ) )

  def dcpt( self, enc ):
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cph = AES.new(self.key, AES.MODE_CBC, iv )
    return unpad(cph.decrypt( enc[16:] ))


cph = AESCipher('veereshkhanorkarnagpurmh')