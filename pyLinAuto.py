#!/usr/local/bin/python2.7
# encoding: utf-8
from commonOs import InstallStatus, renderTemplate, readPWConf, USER, PASS
import os, logging, logging.config, paramiko, time, json
import re, math
from scp import SCPClient

logging.config.dictConfig(json.load(open(os.path.join(os.path.dirname(__file__), "logging.config"))))

class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())


osPat = re.compile("(CentOS|Red).*\s(\d\.\d+).*\((\w+)\).(.*)", re.IGNORECASE)
  

class SSHClient(object):
  nbytes = 4096
  def __init__(self, hostname, username, password):
    self.client = paramiko.SSHClient()
    self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    self.client.connect(hostname, username=username, password=password, timeout=60, auth_timeout=60) 
    self.scpClient = SCPClient(transport=self.client.get_transport(), socket_timeout=120) 
    self.password = password
    self.sudoPrmpt = re.compile('password.*:', re.I)
    self.ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    
  def execRemoteCommand(self, cmd, sudo=False):
    if sudo:
      cmd = "sudo %s" % cmd

    trspt = self.client.get_transport()
    trspt.use_compression(True)
    chan = self.client.get_transport().open_session()
    chan.set_combine_stderr(True)
    chan.get_pty()
    chan.exec_command(cmd)
    chan.setblocking(0)
    CmdOp = []
    pwdSent = False
    errRet = 0
    startTime = time.time()
    currTime = startTime
    while True:
      out = ""
      if chan.recv_ready():
        out = chan.recv(65536)
        out = self.ansi_escape.sub('', out)
        CmdOp.extend(re.split('\r\n',out.strip()))

        if chan.send_ready() and sudo and self.sudoPrmpt.search(''.join(CmdOp)) != None and not pwdSent:
          chan.send(self.password + "\n")
          pwdSent = True
          CmdOp = []

      if chan.exit_status_ready() and len(out) == 0:
        errRet = chan.recv_exit_status()
        break
      elif currTime - startTime > 60:
        errRet = 1
        CmdOp.append("Aborting, the remote server is taking too much time to respond.")
        break
      time.sleep(0.1)
      currTime = time.time()
    return errRet, CmdOp
    
  def execute(self, script, sudo=False, forceContinue=False):
    data = []
    returnCode = 0
    for cmd in script:
      returnCode,o = self.execRemoteCommand(cmd, sudo)
      data.extend(o)
      if not forceContinue:
        if returnCode != 0: break
    return returnCode, data
  
  def close(self):
    self.scpClient.close()
    self.client.close()
    
  def put(self, source, target):
    return self.scpClient.put(source, target)

 
class SSHExecutor(object):
  def __init__(self, data):
    self.hst    = InstallStatus.get(HOSTNAME=data[0])
    self.lock   = data[1]
    self.force  = False
    self.sshClient = None
    
  def useAuth(self, user, pw):
    self.auth = {USER: user, PASS: pw}
    
  def forcefully(self):
    self.force = True


  def installAgent(self):
    if self.hst.DISKSIZE < 200: return
    lines = []
    rc, op = self.sshClient.execute(["mkdir /tmp/moss"])
    logger.debug("Return code for creating folder: %d", rc)
    logger.debug("OP for mkdir: %s", op)
    lines.extend(op)
    self.hst.CREATEFLDRRC = rc
   
    agConfFileName = os.path.join("run","conf","%s_zabbix_agentd.lin.conf" % self.hst.HOSTNAME) 
    agConf = open(agConfFileName,"w")
    agConf.write(renderTemplate('zabbix_agentd.lin.conf', self.hst, self.auth))
    agConf.close()
    self.sshClient.put(agConfFileName, "/tmp/moss/zabbix_agentd.conf")
  
    for filePath in open(os.path.join(os.getcwd(), "run", "{0}_{1}_{2}".format(self.hst.ZBXOS, self.hst.ARCH, "upload.lst"))).readlines():
      p = [a.strip() for a in filePath.split(":")]
      self.sshClient.put(p[0], p[1])
  
    cmds = []
    for cmd in open(os.path.join(os.getcwd(), "run", "{0}_{1}_{2}".format(self.hst.ZBXOS, self.hst.ARCH, "remote.lst"))).readlines():
      cmds.append(cmd.strip())
    logger.debug("Running: {0}".format("\n".join(cmds)))
    r,o = self.sshClient.execute(cmds, sudo=True, forceContinue=self.force)
    lines.extend(o)
      
    with self.lock:
      self.hst.SETUPRC = r
      self.hst.OUTPUTLOG = '\n'.join(lines)
      self.hst.save()

  
  def getData(self):
    logger.info("Processing: %s", self.hst.HOSTNAME)
    auth = readPWConf()
    for user in auth:
      try:
        self.useAuth(user, auth[user])
        self.doActivity()
        break
      except Exception as e:
        self.hst.ACCESSVIA      =  ""
        error = str(e)
        if 'Authentication failed' in error:
          with self.lock:
            self.hst.AUTHFAILED     = True
            self.hst.CONNECTFAILED  = False
            self.hst.save()
        else:
          with self.lock:
            self.hst.CONNECTFAILED  = True
            self.hst.AUTHFAILED     = False
            self.hst.OUTPUTLOG      = error
            self.hst.save()
          break
    logger.info("Processing complete: %s", self.hst.HOSTNAME)
  

  def doActivity(self):
    logger.debug("Inside doActivity: %s", self.hst.HOSTNAME)
    intf = self.hst.IP if self.hst.IP else self.hst.HOSTNAME
    self.sshClient = SSHClient(intf, self.auth[USER], self.auth[PASS])
    with self.lock:
      self.hst.CONNECTFAILED = False
      self.hst.AUTHFAILED=False
      self.hst.OUTPUTLOG = ""
      self.hst.ACCESSVIA = self.auth[USER]
      self.hst.save()
    
    logger.info("Logged in via: %s/%s", self.hst.HOSTNAME, self.auth[USER])
    rc, op = self.sshClient.execute(["ls /tmp"], sudo=True)
    if rc <> 0: 
      with self.lock:
        self.hst.SUDOFAILED = True
        self.hst.OUTPUTLOG = "\n".join(op)
        self.hst.save()
      return
  
    self.hst.SUDOFAILED = False
  
    rc, op = self.sshClient.execute(["rpm -qa | grep zabbix-agent"], sudo=True)
    op = ''.join(a.strip() for a in op)
    logger.debug("Agent: %s", op)
  
    with self.lock:
      self.hst.AGENTEXIST = op
      self.hst.save()
  
    if not self.force:
      if op <> "": return
      
    for fs in ["/tmp", "/"]:
      rc, op = self.sshClient.execute(["df %s | awk '{print $3}' | tail -1" % fs], sudo=True)
      op = ''.join(a.strip() for a in op)
      logger.debug("Disk space: %s", op)
      diskSize = long(op)/1024*1024
      with self.lock:
        if self.hst.DISKSIZE == None:
          self.hst.DISKSIZE = diskSize
        else:
          self.hst.DISKSIZE = self.hst.DISKSIZE if self.hst.DISKSIZE < diskSize else diskSize
        
        self.hst.save()
      logger.debug("Free Size: %d MB", diskSize)
    
    rc, op = self.sshClient.execute(["echo `cat /etc/redhat-release` `arch`"], sudo=True)
    op = ''.join(a.strip() for a in op)
    logger.debug("OS Version: %s", op)
    with self.lock:
      self.hst.OSVERSION = op
      m = osPat.match(op)
      if m:
        if m.group(1) in ['CentOS', 'Red']:
          self.hst.ZBXOS = "RHEL"
          self.hst.ZBXOS += str(int(math.floor(float(m.group(2))))) + ".x"
          self.hst.ARCH = m.group(4)
      self.hst.save()
      
  def close(self):
    if self.sshClient: self.sshClient.close()
    logger.debug("Returning from doActivity: %s", self.hst.HOSTNAME)

def GetData(data):
  task = SSHExecutor(data)
  task.getData()
  task.close()
  
def InstallAgent(data):
  task = SSHExecutor(data)
  task.getData()
  task.installAgent()
  task.close()

def InstallAgentForcefully(data):
  task = SSHExecutor(data)
  task.getData()
  task.forcefully()
  task.installAgent()