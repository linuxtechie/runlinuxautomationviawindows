#!/usr/local/bin/python2.7
# encoding: utf-8
from commonOs import InstallStatus, renderTemplate, PASS, USER, readPWConf
import logging, logging.config, json, os, csv, winrm, jinja2, tempfile
from subprocess import call

logging.config.dictConfig(json.load(open(os.path.join(os.path.dirname(__file__), "logging.config"))))

class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())
  
class WinExecutor(object):
  def __init__(self, data):
    self.hst    = InstallStatus.get(HOSTNAME=data[0])
    self.lock   = data[1]
    self.force  = False
    self.winrmClient = None
    
  def useAuth(self, user, pw):
    self.auth = {USER: user, PASS: pw}
    
  def forcefully(self):
    self.force = True
    
  def getData(self):
    logger.info("Processing: %s", self.hst.HOSTNAME)
    auth = readPWConf()
    for user in auth:
      try:
        self.useAuth(user, auth[user])
        self.doActivity()
        break
      except Exception as e:
        self.hst.ACCESSVIA      =  ""
        error = str(e)
        if 'the specified credentials were rejected by the server' in error:
          with self.lock:
            self.hst.AUTHFAILED     = True
            self.hst.CONNECTFAILED  = False
            self.hst.save()
        else:
          with self.lock:
            self.hst.AUTHFAILED     = False
            self.hst.CONNECTFAILED  = True
            self.hst.OUTPUTLOG      = error
            self.hst.save()
          break
    logger.info("Processing complete: %s", self.hst.HOSTNAME)
  
  def getDiskSpace(self):
    ps_script = """$disk = Get-WmiObject Win32_LogicalDisk -Filter "DeviceID='C:'" | Select-Object Size,FreeSpace 
$disk.FreeSpace""".format(self.hst.HOSTNAME)
    r = self.winrmClient.run_ps(ps_script)
    diskSize = long(r.std_out) / (1024 * 1024)
    return diskSize
  
  def getHostName(self):
    ps_script = """(Get-WmiObject -Class Win32_ComputerSystem -Property Name).Name"""
    r = self.winrmClient.run_ps(ps_script)
    return r.status_code, r.std_out
  
  def checkAgentExist(self):
    ps_script = '''$serviceName = 'Zabbix Agent'
If (Get-Service $serviceName -ErrorAction SilentlyContinue) {
    If ((Get-Service $serviceName).Status -eq 'Running') {
        Write-Host "RUNNING"
    } Else {
        Write-Host "NOTRUNNING"
    }
} Else {
    Write-Host "NOTFOUND"
}
'''
    r = self.winrmClient.run_ps(ps_script)
    return r.status_code, r.std_out
  
  def doActivity(self):
    logger.debug("Inside doActivity: %s", self.hst.HOSTNAME)
    intf = self.hst.IP if self.hst.IP else self.hst.HOSTNAME
    self.winrmClient = winrm.Session(intf, (self.auth[USER], self.auth[PASS]), server_cert_validation='ignore', transport='ntlm')
    with self.lock:
      self.hst.CONNECTFAILED  = False
      self.hst.AUTHFAILED     = False
      self.hst.ACCESSVIA      = self.auth[USER]
      self.hst.OUTPUTLOG      = ""
      self.hst.save()

    rc, op = self.getHostName()
    self.hst.NAME = op.strip()
    rc, op = self.checkAgentExist()
    op = op.strip()
    self.hst.AGENTEXIST       = op
    if op != "RUNNING": 
      self.hst.DISKSIZE         = self.getDiskSpace()
    with self.lock:
      self.hst.save()
    logger.info("Logged in via: %s/%s", self.hst.HOSTNAME, self.auth[USER])
    logger.debug("Returning from doActivity: %s", self.hst.HOSTNAME)
      

  def installAgent(self):
    if self.hst.DISKSIZE < 200: return
    if not self.forcefully():
      if self.hst.AGENTEXIST != "NOTFOUND": return
    
    lines = []
    r = self.winrmClient.run_cmd("MKDIR C:\MOSS")
    logger.info("Return code for creating folder: %d", r.status_code)
    logger.info("OP for mkdir: %s", r.std_out)
    lines.append(r.std_out)
    self.hst.CREATEFLDRRC = r.status_code
  
    agConf = open(os.path.join("run","%s_zabbix_agentd.win.conf" % self.hst.HOSTNAME),"w")
    agConf.write(
      jinja2.Environment(loader=jinja2.FileSystemLoader('./run/')).get_template('zabbix_agentd.win.conf').render({
            'HOSTNAME': self.hst.HOSTNAME
          }
        )
      )
    agConf.close()
    returnCode = 0
    f = tempfile.TemporaryFile()
    for cmd in renderTemplate('local.bat', self.hst.HOSTNAME, self.auth).splitlines():
      f.write("Executing: %s\n" % cmd)
      f.flush()
      logger.info("Running: %s", cmd)
      returnCode = call(cmd, stdout=f,stderr=f)
      logger.info("Return Code: %d", returnCode)
      if returnCode != 0:
        break
    f.seek(0)
    lines.append(f.read())
    f.close()
    self.hst.COPYFILERC = returnCode
    
    if returnCode == 0: 
      for cmd in renderTemplate('remote.bat', self.hst.HOSTNAME, self.auth).splitlines():
        r = self.winrmClient.run_cmd(cmd)
        lines.append("Running: {0}".format(cmd))
        lines.append(r.std_out)
        returnCode = r.status_code
        if r.status_code != 0:
          break
      self.hst.SETUPRC = returnCode
  
    with self.lock:
      self.hst.OUTPUTLOG = ' '.join(lines)
      self.hst.save()
      
  def close(self):
    pass


def GetData(data):
  task = WinExecutor(data)
  task.getData()
  task.close()
  
def InstallAgent(data):
  task = WinExecutor(data)
  task.getData()
  task.installAgent()
  task.close()

def InstallAgentForcefully(data):
  task = WinExecutor(data)
  task.getData()
  task.forcefully()
  task.installAgent()  