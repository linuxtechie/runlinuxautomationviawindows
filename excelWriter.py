'''
Created on Dec 20, 2016

@author: ekhavee
'''
import os
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Border, Side, Alignment, Protection, Font, Color
import logging

class _NullHandler(logging.Handler):
    def emit(self, record):
        pass

logger = logging.getLogger(__name__)
logger.addHandler(_NullHandler())


class ExcelWriter(object):
  def __init__(self, exclName):
    if os.path.isfile(exclName): os.remove(exclName)
    self.wb = Workbook()
    self.exclName = exclName
    self.cols = ['hostgroup', 'template', 'application', 'name', 'description', 'delay', 'history','trends', 'key', 'snmp_oid',  'type', 'value_type', 'data_type', 'status', 'units', 'valuemap']
    self.triggercols = ['hostgroup', 'template', 'name', 'description', 'expression', 'priority','type', 'status']
    self.valueMapCols = ['hostgroup', 'template', 'name', 'value', 'newvalue']
    self.fFont = Font(name='Ericsson Hilda',
                 size=9,
                 bold=True,
                 italic=False,
                 vertAlign=None,
                 underline='none',
                 strike=False,
                 color='FF000000')
    self.cFont = Font(name='Ericsson Hilda',
                 size=9,
                 bold=False,
                 italic=False,
                 vertAlign=None,
                 underline='none',
                 strike=False,
                 color='FF000000')

  def getWorkSheetWCols(self, ws, cols, position=None):
    self.cols = cols
    return self.getWorkSheet(ws, Triggers=False, Items=True, ValueMap=False, position=position)
  
  def getWorkSheet(self, ws, Triggers=False, Items=True, ValueMap=False, position=None):
    if ws in self.wb.sheetnames:
      worksheet = self.wb[ws]
    else:
      if position is not None:
        worksheet = self.wb.create_sheet(ws, index=position)
      else:
        worksheet = self.wb.create_sheet(ws)
    
    cols = None
    if Triggers: cols = self.triggercols
    if Items: cols = self.cols
    if ValueMap: cols = self.valueMapCols
      
    i = 1
    for c in cols:
      if c == "delay": c = "INTERVAL (SEC)"
      cell = worksheet.cell(row=1, column=i, value=c.upper())
      cell.font = self.fFont
      cell.fill=PatternFill(patternType='solid',
                                      fill_type='solid', 
                                      fgColor=Color('FFFF00'))
      i = i + 1
    return worksheet

  def autoCloumnWidth(self, worksheet):
    for col in worksheet.columns:
      max_length = 0
      column = col[0].column  # Get the column name
      for cell in col:
        try:  # Necessary to avoid error on empty cells
          if len((str(cell.value)).strip()) > max_length:
            max_length = len(cell.value)
        except:
            pass
      adjusted_width = (max_length) * 1.2
      if adjusted_width > 35:
        adjusted_width = 35
      worksheet.column_dimensions[column].width = adjusted_width
      logger.info("%s Max length: %d and width: %d", column, max_length, adjusted_width)
          
  def Close(self):
    for ws in self.wb.sheetnames:
      self.autoCloumnWidth(self.wb[ws])
    try:
      s = self.wb["Sheet"]
      self.wb.remove(s)
    except Exception:
      pass
    self.wb.save(self.exclName)

  
  def getCols(self):
    return self.cols
  

  def writeCell(self, ws, row, col, value, wrap=False):
    cell = ws.cell(row=row, column=col, value=value)
    cell.font = self.cFont
    if wrap:
      cell.alignment = Alignment(wrapText=True)
